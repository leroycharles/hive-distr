#/!bin/bash
## @file HIVE.sh
## @brief Main file contening all functions calls and declaration
## @details This file contains all functions and scripts calls.
## @author Charles Leroy

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit 1
fi

#=========================================CHROOT=========================================#
## @fn chrootCommons()
## @brief Create and add essential.
## @details Create mount points and chroot the commons install script.
## @author Charles Leroy
function chrootCommons(){
	mount --bind /proc $1/live/squashfs/proc 
	mount --bind /sys $1/live/squashfs/sys
	mount -t devpts none $1/live/squashfs/dev/pts
	cp -uv /etc/resolv.conf $1/live/squashfs/etc/resolv.conf

	chroot $1/live/squashfs bash $1/config/ch_commons.sh
	return $?
}

## @fn chrootServer()
## @brief Add essential for server.
## @details Chroot the server install script.
## @author Charles Leroy
function chrootServer(){
	chroot $1/live/squashfs bash $1/config/ch_server.sh
	return $?
}

## @fn chrootDesktop()
## @brief Add essential for desktop.
## @details Chroot the desktop install script.
## @author Charles Leroy
function chrootDesktop(){
	chroot $1/live/squashfs bash $1/config/ch_server.sh
	return $?
}


#=========================================ISO=========================================#
## @fn updateUISO()
## @brief Update Ubuntu ISO from releases.
## @details Update specified ISO file with zsync.
## @author Charles Leroy
function updateUISO(){
	if [ $1 == "0" ]
	then
		output= updateUISO "1" $2
		output= updateUISO "2" $2
	elif [ $1 == "2" ]
	then 
		if zsync http://releases.ubuntu.com/focal/ubuntu-20.04-desktop-amd64.iso.zsync -o $2/iso/u_desktop.iso; then
			return 0
		else
			exit 1
		fi
	elif [ $1 == "1" ]
	then
		if zsync http://releases.ubuntu.com/focal/ubuntu-20.04-live-server-amd64.iso.zsync -o $2/iso/u_server.iso; then
			return 0
		else
			exit 1
		fi
	else
		exit 1
	fi
}

## @fn prepareUISO()
## @brief Open Ubuntu ISO for edit.
## @details Extract ISO files and prepare tehm for edition.
## @author Charles Leroy
function prepareUISO(){
	if [ $1 == "2" ]
	then
		isoName="u_desktop.iso"
	elif [ $1 == "1" ]
	then
		isoName="u_server.iso"
	fi
	mount -o loop $2/iso/$isoName /mnt
	cp -av /mnt/. $2/live/iso
	umount /mnt

	mount -t squashfs -o loop $2/live/iso/casper/filesystem.squashfs /mnt
	cp -auv /mnt/. $2/live/squashfs
	umount /mnt
	return 0
}

## @fn makeHISO()
## @brief Create ISO file.
## @details Create specified ISO file and make a .zsync available for this file.
## @author Charles Leroy
function makeHISO(){
	chmod a+w $1/live/iso/casper/filesystem.manifest
	chroot $1/live/squashfs dpkg-query -W --showformat='${Package}  ${Version}\n' > $1/live/iso/casper/filesystem.manifest
	chmod go-w $1/live/iso/casper/filesystem.manifest

	rm $1/live/iso/casper/filesystem.squashfs
	mksquashfs $1/live/squashfs $1/live/iso/casper/filesystem.squashfs -info

	## Updating vmlinuz and initrd.lz here

	cd $1/live/iso
	bash -c "find . -path ./isolinux -prune -o -type f -not -name md5sum.txt -print0 | xargs -0 md5sum | tee md5sum.txt"

	mkisofs -U -A "Custom" -V "Custom" -volset "Custom" -J -joliet-long -r -v -T -o $1/iso/tmp_cus.iso -b isolinux/isolinux.bin -c isolinux/boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table -eltorito-alt-boot -e boot/grub/efi.img -no-emul-boot .
	isohybrid -u $1/iso/tmp_cus.iso
	if [ $2 == "1" ]
	then
		mv $1/iso/tmp_cus.iso $2/iso/u_server.iso
	else
		mv $1/iso/tmp_cus.iso $2/iso/u_desktop.iso
	fi
	return 0
}


#=========================================MAIN=========================================#

apt-get update -q
apt-get install -qy squashfs-tools schroot genisoimage syslinux-utils zsync
if [ -n "$1" ]
then
	uInput=$1
else
	clear
	echo "HIVE DISTRIBUTION V0.0.0"
	echo "Select Option:
	0-Updating Ubuntu ISO File
	1-Updating HIVE Server
	2-Updating HIVE Desktop

	5-Exit"
	read uInput
fi

if [ $uInput == "5" ]
then
	exit
elif [ $uInput == 0 ]
then
	updateUISO 0 $(dirname "$0")
	exit
else
	updateUISO $uInput $(dirname "$0")
	prepareUISO $uInput $(dirname "$0")
	chrootCommons $(dirname "$0")
	#if [ uInput == "1" ]
	#then
	#	chrootServer $(dirname "$0")
	#	#installServer $(dirname "$0")
	#else
	#	chrootDesktop $(dirname "$0")
	#	#installDesktop $(dirname "$0")
	#fi
	makeHISO $(dirname "$0") $uInput
	exit
fi
