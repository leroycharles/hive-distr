# HIVE Distribution
## HIVE-U Desktop
Ubuntu Desktop Distribution customise to interact with HIVE systems (servers, mobiles, IoT, ..) with a graphic interface and for daily personal and professional uses.

## HIVE-U Server
Ubuntu Server Distribution customise to manage HIVE systems (laptops, servers, mobiles, IoT, ...). Default is without graphical interface.

## NOTES
This floder contains the Jupyter Notebooks used for the developpement of the solution.
They can be access throught the "Related Pages" section of the Doxygen Documentation.